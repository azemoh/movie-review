class Movie < ActiveRecord::Base
  belongs_to :user
  has_many :reviews

  def slug
    self.title.gsub(" ", "-").downcase
  end

  def self.find_by_slug(slug)
    title = slug.gsub("-", " ")
    self.where("lower(title) = ?", title.downcase).limit(1).first
  end

  def rating
    reviews.average(:rating)
  end
end
