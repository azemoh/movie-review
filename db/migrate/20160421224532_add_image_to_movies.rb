class AddImageToMovies < ActiveRecord::Migration
  def change
    change_table :movies do |t|
      t.string :image_url
    end
  end
end
