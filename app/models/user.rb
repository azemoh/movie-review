class User < ActiveRecord::Base
  has_secure_password
  has_many :movies
  has_many :reviews

  def self.find_by_username_or_email(username)
    User.where("username = ? OR email = ?", username, username).first
  end
end