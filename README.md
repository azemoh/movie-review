## Movie Reviewer

This is my Sinatra assessment project for Learn-Verified. It is a Movie review application where users can post movies and others can write reviews about them.
Read about it [here](http://azemoh.com/2016/04/30/movie-review-learn-sinatra-project.html)

## Set Up

- Install gems
```
bundle install
```

- Run Migrations
```
rake db:migrate
```

- Import seed data
```
rake db:seed
```

- Start server
```
rackup config.ru
```
