require_relative '../spec_helper'

describe "Movie" do
  before do
    @movie = Movie.create(title: "Daddy's Home", description: "Stepdad, Brad Whitaker, is a radio host trying to get his stepchildren to love him and call him Dad. But his plans turn upside")
  end

  it "has a title" do
    expect(@movie.title).to eq("Daddy's Home")
  end

  it "has a description" do
    expect(@movie.description).to eq("Stepdad, Brad Whitaker, is a radio host trying to get his stepchildren to love him and call him Dad. But his plans turn upside")
  end

  it "has a user" do
    @movie.user = User.create(username: "dean", password: "74yurh")
    @movie.save
    expect(@movie.user.username).to eq("dean")
  end
end

describe "User" do
  before do
    @sam = User.create(username: "sam", email: "sam@gmail.com", password: "password")
  end

  it "can create user" do
    expect(@sam.username).to eq("sam")
  end

  it "can authenticate" do
    expect(@sam.authenticate("password")).to be_truthy
  end

  it "has movies" do
    @sam.movies.build(title: "Star Wars", description: "Lorem ipsum lorem djeh")
    @sam.save
    expect(@sam.movies.size).to eq(1)
  end

  it "has reviews" do
    @sam.reviews.build(rating: 4, comment: "La la la la.")
    @sam.save
    expect(@sam.reviews.size).to eq(1)
  end

  describe ".find_by_username_or_email" do
    it "can be found with username" do
      user = User.find_by_username_or_email("sam")
      expect(user.email).to eq(@sam.email)
    end

    it "can be found with email" do
      user = User.find_by_username_or_email("sam@gmail.com")
      expect(user.username).to eq(@sam.username)
    end
  end
end

describe "Review" do
  it "Can be created" do
    Review.create(comment: "This movie sucks!")
    expect(Review.last.comment).to eq("This movie sucks!")
  end
end
