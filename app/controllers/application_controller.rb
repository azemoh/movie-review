class ApplicationController < Sinatra::Base
  configure do
    set :views, 'app/views'
    enable :sessions
    set :session_secret, "superduppersecrete"
    set :public_folder, "public"
  end

  get '/' do
    @movies = Movie.all
    erb :index
  end

  get '/signup' do
    erb :signup
  end

  post '/signup' do
    unless user_fields_empty?
      user = User.create(params[:user])
      if user
        session[:user_id] = user.id
        redirect '/'
      end
    end

    erb :signup
  end

  get '/login' do
    erb :login
  end

  post '/login' do
    user = User.find_by_username_or_email(params[:username])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect '/'
    end

    erb :login
  end

  get '/logout' do
    session.clear
    redirect '/login'
  end

  get '/users/:username' do
    @user = User.find_by(username: params[:username])
    erb :profile
  end

  helpers do
    def logged_in?
      !!session[:user_id]
    end

    def current_user
      User.find(session[:user_id])
    end

    def pluralize(word, no)
      return word if no < 2
      last_letter = word[-1].downcase

      if word[-3..-1] == "ice"
        return word[0..-4] + "ouse"
      elsif last_letter == "y"
        return word[0..-2] + "ies"
      elsif last_letter == "s"
        return word[0..-2] + "es"
      else
        return word + "s"
      end
    end
  end

  def user_fields_empty?
    user_hash = params[:user]
    user_hash[:username].empty? || user_hash[:password].empty? || user_hash[:email].empty?
  end
end