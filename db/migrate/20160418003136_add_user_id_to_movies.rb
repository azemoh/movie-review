class AddUserIdToMovies < ActiveRecord::Migration
  def change
    change_table :movies do |t|
      t.integer :user_id
    end
  end
end
