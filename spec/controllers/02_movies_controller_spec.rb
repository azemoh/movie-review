require_relative "../spec_helper"

describe MoviesController do
  before do
    @user = User.create(username: "angelo", email: "angelo@yahoo.com", password: "angelo74j")
    @params = {movie: {title: "Batman v Superman: Dawn of Justice", description: "Batman v Superman: Dawn of Justice Description"}}
  end

  describe "Show Movie" do
    before do
      @movie = Movie.create(@params[:movie])
      @movie.update(user_id: @user.id)
      get "movies/#{@movie.slug}"
    end

    it "Shows the movie details" do
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include(@movie.title)
    end

    it "does no show edit link if the movie does not belong to current_user" do
      User.create(username: "sam", email: "sam@mail.com", password: "password")
      post "/login", {username: "sam", password: "password"}
      get "movies/#{@movie.slug}"
      expect(last_response.body).not_to include("/movies/#{@movie.slug}/edit")
    end

    context "Logged out" do
      it "does not displays review form" do
        expect(last_response.body).not_to include("<form")
        expect(last_response.body).not_to include("/movies/#{@movie.slug}/review")
      end
    end

    context "Logged in" do
      before do
        visit "/login"
        fill_in :username, with: "angelo"
        fill_in :password, with: "angelo74j"
        click_button "Login"
        visit "movies/#{@movie.slug}"
      end

      it "displays review form" do
        expect(page.body).to include("<form")
        expect(page.body).to include("/movies/#{@movie.slug}/review")
      end

      it "submit review" do
        fill_in :comment, with: "Great movie"
        find('#rating3').click
        click_button "Submit"
        expect(@movie.reviews.size).to eq(1)
        expect(page.body).to include("Great movie")
      end
    end
  end

  describe "New movie:" do
    context "logged out" do
      it "does not let a user create a movie" do
        get "/movies/new"
        expect(last_response.location).to include("/login")
      end
    end
    context "logged in" do
      before do
        post "/login", {username: "angelo", password: "angelo74j"}
      end

      it "load new movie page" do
        get "/movies/new"
        expect(last_response.status).to eq(200)
      end

      it "creates a new movie and associate it to the logged in user" do
        post "/movies", @params
        movie = Movie.last
        expect(movie.user.username).to eq(@user.username)
      end

      it "creates a new movie and displays it" do
        post "/movies", @params
        movie = Movie.last
        expect(movie.title).to eq("Batman v Superman: Dawn of Justice")
        expect(last_response.location).to include("/movies/#{movie.slug}")
      end
    end
  end

  describe "Edit Movie" do
    context "Logged out" do
      it "does not let a user view the edit page" do
        get "/movies/new"
        expect(last_response.location).to include("/login")
      end
      it "does not let a user edit a movie" do
        patch "/movies/the-slug", @params
        expect(last_response.location).to include("/login")
      end
    end

    context "Logged in" do
      before do
        visit "/login"
        fill_in :username, with: "angelo"
        fill_in :password, with: "angelo74j"
        click_button "Login"
        @movie = Movie.create(@params[:movie])
        @movie.update(user_id: @user.id)
        visit "movies/#{@movie.slug}/edit"
      end

      it "Edits a movie and display its details" do
        fill_in :title, with: "Batman v Superman Dawn of Justice II"
        fill_in :description, with: "New description"
        click_button "Save"
        expect(page.body).to include("New description")
        expect(page.body).to include("Batman v Superman Dawn of Justice II")
      end
      context "Not current_user's movie" do
        before do
          User.create(username: "sam", email: "sam@gmail.com", password: "password")
          post "/login", {username: "sam", password: "password"}
        end

        it "does not allow user see the edit page" do
          get "movies/#{@movie.slug}/edit"
          expect(last_response.location).to include("/login")
        end

        it "does not allow user edit the movie" do
          patch "movies/#{@movie.slug}", @params
          expect(last_response.location).to include("/login")
        end
      end
    end
  end
end
