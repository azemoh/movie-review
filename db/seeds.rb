# Database seed

sam  = User.create(username: "sam", email: "sam@mail.com", password: "password")
joe  = User.create(username: "joe", email: "joe@mail.com", password: "passW0rd")

# Movies
sam.movies.build(title: "The Martian", image_url: "http://ia.media-imdb.com/images/M/MV5BMTc2MTQ3MDA1Nl5BMl5BanBnXkFtZTgwODA3OTI4NjE@._V1_UY209_CR0,0,140,209_AL_.jpg",description: "During a manned mission to Mars, Astronaut Mark Watney is presumed dead after a fierce storm and left behind by his crew. But Watney has survived and finds himself stranded and alone on the hostile planet. With only meager supplies, he must draw upon his ingenuity, wit and spirit to subsist and find a way to signal to Earth that he is alive.")
sam.movies.build(title: "Daddy's Home", image_url: "http://ia.media-imdb.com/images/M/MV5BMTQ0OTE1MTk4N15BMl5BanBnXkFtZTgwMDM5OTk5NjE@._V1_UX140_CR0,0,140,209_AL_.jpg", description: "Stepdad, Brad Whitaker, is a radio host trying to get his stepchildren to love him and call him Dad. But his plans turn upside down when the biological father, Dusty Mayron, returns.")
sam.save

joe.movies.build(title: "The Revenant", image_url: "http://ia.media-imdb.com/images/M/MV5BMjU4NDExNDM1NF5BMl5BanBnXkFtZTgwMDIyMTgxNzE@._V1_UY209_CR0,0,140,209_AL_.jpg",description: "A frontiersman on a fur trading expedition in the 1820s fights for survival after being mauled by a bear and left for dead by members of his own hunting team.")
joe.movies.build(title: "The Big Short", image_url: "http://ia.media-imdb.com/images/M/MV5BMjM2MTQ2MzcxOF5BMl5BanBnXkFtZTgwNzE4NTUyNzE@._V1_UX140_CR0,0,140,209_AL_.jpg",description: "Four denizens in the world of high-finance predict the credit and housing bubble collapse of the mid-2000s, and decide to take on the big banks for their greed and lack of foresight.")
joe.save

# Reviews
sam.reviews.build(movie_id: joe.movies.last.id, rating: 3, comment: "The movie is meh... :/")
sam.reviews.build(movie_id: joe.movies.first.id, rating: 5, comment: "Wow Awesome movie.")
sam.save

joe.reviews.build(movie_id: sam.movies.last.id, rating: 4, comment: "Good movie!")
joe.reviews.build(movie_id: sam.movies.first.id, rating: 5, comment: "I could watch this forever :p.")
joe.save