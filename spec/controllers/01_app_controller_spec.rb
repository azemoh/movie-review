require_relative '../spec_helper'

describe ApplicationController do
  describe "Signup Page" do
    it "has a signup page" do
      get "/signup"
      expect(last_response.status).to eq(200)
    end

    it "signs a user up and redirect to index" do
      visit "/signup"

      fill_in :username, with: "angelo"
      fill_in :email, with: "angelo@yahoo.com"
      fill_in :password, with: "angelo74j"
      click_button "Signup"

      expect(User.last.username).to eq("angelo")
      expect(page.current_path).to eq("/")
    end

    it "does not allow a user signup without a username" do
      visit "/signup"

      fill_in :email, with: "angelo@yahoo.com"
      fill_in :password, with: "angelo74j"
      click_button "Signup"

      expect(page.current_path).to eq("/signup")
    end

    it "does not allow a user signup without an email address" do
      visit "/signup"

      fill_in :username, with: "angelo"
      fill_in :password, with: "angelo74j"
      click_button "Signup"

      expect(page.current_path).to eq("/signup")
    end

    it "does not allow a user signup without a password" do
      visit "/signup"

      fill_in :username, with: "angelo"
      fill_in :email, with: "angelo@yahoo.com"
      click_button "Signup"

      expect(page.current_path).to eq("/signup")
    end
  end

  describe "Login Page" do
    before do
      User.create(username: "angelo", email: "angelo@yahoo.com", password: "angelo74j")
    end

    it "has a login page" do
      get "/signup"
      expect(last_response.status).to eq(200)
    end

    it "logs a user in and redirect to index" do
      visit "/login"

      fill_in :username, with: "angelo"
      fill_in :password, with: "angelo74j"
      click_button "Login"

      expect(page.current_path).to eq("/")
    end

    it "does not allow invalid logins" do
      visit "/login"

      fill_in :username, with: "angelo"
      fill_in :password, with: "angel074j"
      click_button "Login"

      expect(page.current_path).to eq("/login")
    end
  end
end