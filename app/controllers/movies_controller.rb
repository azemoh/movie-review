class MoviesController < ApplicationController
  get '/movies' do
    redirect '/'
  end

  get '/movies/new' do
    redirect '/login' unless logged_in?
    erb :"/movies/new"
  end

  post '/movies' do
    redirect '/login' unless logged_in?
    @movie = Movie.create(params[:movie])
    @movie.user = current_user
    save_movie_image if movie_image_uploaded?
    @movie.save
    redirect "/movies/#{@movie.slug}"
  end

  get '/movies/:slug' do
    @movie = Movie.find_by_slug(params[:slug])
    erb :"/movies/show"
  end

  get '/movies/:slug/edit' do
    redirect '/login' unless logged_in?
    @movie = Movie.find_by_slug(params[:slug])
    redirect '/login' unless @movie.user == current_user
    erb :"/movies/edit"
  end

  patch '/movies/:slug' do
    redirect '/login' unless logged_in?
    @movie = Movie.find_by_slug(params[:slug])
    redirect '/login' unless @movie.user == current_user
    if @movie.update(params[:movie])
      if movie_image_uploaded?
        delete_movie_image if @movie.image_url
        save_movie_image
        @movie.save
      end
      redirect "/movies/#{@movie.slug}"
    end
    erb :"/movies/edit"
  end

  post '/movies/:slug/review' do
    redirect '/login' unless logged_in?
    @movie = Movie.find_by_slug(params[:slug])
    @movie.reviews.build(params[:review])
    @movie.save
    redirect "/movies/#{@movie.slug}"
  end

  private
    def save_movie_image
      @movie.image_url = "/uploads/#{@movie.slug}-#{params[:movie_image][:filename]}"
      file = params[:movie_image][:tempfile]
      File.open("./public#{@movie.image_url}", "wb") do |f|
        f.write(file.read)
      end
    end
 
    def delete_movie_image
      path = "./public#{@movie.image_url}"
      File.delete(path) if File.exist?(path)
    end

    def movie_image_uploaded?
      params[:movie_image] && params[:movie_image][:filename]
    end
end
